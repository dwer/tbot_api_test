from telegram.ext import Updater, CommandHandler, Filters, MessageHandler
from collections import deque
import db


class Bot:

    def __init__(self, token):
        self.token = token

    def _collect_dialog(self, chat_id, message_id):
        dialog = deque()
        # request last 500 messages (starting from quoted)
        limit = 500
        history = dict(db.retrieve_history(chat_id, message_id, limit))
        while (message_id != None):
            dialog.appendleft(message_id)
            message_id = history[message_id]
            # in case 500 messages not enough to collect dialog, request another 500
            if limit <= 0:
                limit = 500
                history = dict(db.retrieve_history(chat_id, message_id, limit))
            limit -= 1;
        return dialog

    def help(self, bot, update):
        """Shows list of commands/how to use guide"""
        chat_id = update.message.chat_id
        bot.send_message(chat_id=chat_id,
                         text="In order to get dialog from chat, add me as a contact.\n"
                              "Quote message and pass a /dialog command")

    def get_dialog(self, bot, update):
        """Sends dialog to a user.
        User should add a bot as contact. This is required to not spam the chat with messages
        """
        message = update.message.reply_to_message  # we're interested in quoted message as an endpoint
        chat_id = message.chat_id
        message_id = message.message_id
        dialog = self._collect_dialog(chat_id, message_id)
        for message_id in dialog:
            bot.forward_message(chat_id = update.effective_user.id, from_chat_id=chat_id, message_id=message_id)

    def save_history(self, bot, update):
        """Stores new messages to database
        Only message id and quoted message id items are stored
        """
        message = update.message
        id = message.message_id
        quoted = message.reply_to_message
        if (quoted is not None):
            quoted = quoted.message_id
        db.insert_into_table(message.chat_id, id, quoted)

    def run(self):
        """Register handlers and start listen"""
        updater = Updater(self.token)
        dispatcher = updater.dispatcher
        dispatcher.add_handler(CommandHandler('help', self.help))
        dispatcher.add_handler(CommandHandler('dialog', self.get_dialog))
        #consider on handling all messages within single handler
        updater.dispatcher.add_handler(MessageHandler(Filters.text, self.save_history))
        updater.dispatcher.add_handler(MessageHandler(Filters.voice, self.save_history))
        updater.start_polling()
        updater.idle()


if __name__ == '__main__':
   Bot("787904086:AAERU4e-lbBFr79X2oUEoiB3s-4RrgAGJkc").run()