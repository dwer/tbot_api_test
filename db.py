import sqlite3

""" TODO: use sqlite alchemy + handle multithreading;
    Current multithreading problem: I init connection in each method in order to allow bot threads to use it 
    instead of global declaration
    conn = sqlite3.connect("history_map.db")
    cursor = conn.cursor()
"""

DB = "history_map.db"


def _insert_into_table(name, message_id, quoted_message_id):
    with sqlite3.connect(DB) as conn:
        cursor = conn.cursor()
        cursor.execute("INSERT INTO '{0}' VALUES ({1}, {2})".format(name, message_id, quoted_message_id))
        conn.commit()


def generate_table(name):
    """creates new historic map table in database with chosen name"""
    with sqlite3.connect(DB) as conn:
        cursor = conn.cursor()
        cursor.execute(
            "CREATE TABLE '{0}' (id INTEGER PRIMARY KEY, quoted INTEGER, FOREIGN KEY(quoted) REFERENCES '{0}' (id))"
                .format(name))


def insert_into_table(name, message_id, quoted_message_id):
    """adds new item into table"""
    if quoted_message_id is None: quoted_message_id = 'null'
    try:
        _insert_into_table(name, message_id, quoted_message_id)
    except sqlite3.OperationalError:
        generate_table(name)
        _insert_into_table(name, message_id, quoted_message_id)
    except Exception as e:
        print(e)
        raise


def is_table_exist(name):
    """checks if table exists"""
    with sqlite3.connect(DB) as conn:
        cursor = conn.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{0}'".format(name));
        return cursor.rowcount > 0;


def retrieve_history(table_name, from_message_id, max_lines):
    with sqlite3.connect(DB) as conn:
        cursor = conn.cursor()
        if (max_lines > from_message_id):
            cursor.execute("SELECT * from '{0}' WHERE id <= {1}".format(table_name, from_message_id))
        else:
            cursor.execute("SELECT * from '{0}' WHERE id BETWEEN {1} AND {2}"
                           .format(table_name, from_message_id-max_lines, from_message_id))
        return cursor.fetchall()